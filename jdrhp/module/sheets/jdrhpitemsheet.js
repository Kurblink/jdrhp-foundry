export default class JdrhpItemSheet extends ItemSheet{
    get template(){
        console.log(`Jdrhp | Récupération du fichier html ${this.item.data.type}-sheet.`);

        return `systems/jdrhp/templates/sheets/item/${this.item.data.type}-sheet.html`;
    }
    
    getData(){
        const data = super.getData();

        console.log(data);

        return data;
    }
}