import JdrhpItemSheet from "./sheets/JdrhpItemSheet.js";
import JdrhpActorSheet from "./sheets/JdrhpActorSheet.js";

Hooks.once("init", () => {
    console.log("Jdrhp | Initialisation du système Jdrhp");

    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("jdrhp", JdrhpItemSheet, { makeDefault: true });

    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("jdrhp", JdrhpActorSheet, { makeDefault: true })
})

//let titles = document.querySelectorAll('#tabs .tab-titles a');

//for(let title of titles) {
  //title.addEventListener('click', fonction(){
    // On récupère le numéro du titre
    //let num = this.getAttribute('data-tabtitle'),

    //document.querySelector('#tabs > div > div.active').classList.remove('active')
  //});
//}